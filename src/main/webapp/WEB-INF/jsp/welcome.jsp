<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
<div>
    <div>
        <h1>Group 4 SDA Spring boot JSP example</h1>
        <h2>${message}</h2>

        Click on this <strong><a href="next">link</a></strong> to visit another page.
    </div>
</div>
</body>
</html>