<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
<div>
    <div>
        <h1>This is another page</h1>
        <h2>${message}</h2>

        Click on this <strong><a href="/">link</a></strong> to visit previous page.
    </div>
</div>
</body>
</html>