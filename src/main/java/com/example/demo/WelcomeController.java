package com.example.demo;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {


    private String message = "Hello Group 4";

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        model.put("message", this.message);
        return "welcome";
    }

    @RequestMapping("/next")
    public String next(Map<String, Object> model) {
        model.put("message", "This message was set from the controller");
        return "next";
    }

}
